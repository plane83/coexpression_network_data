Updated on March. 1, 2016.
Contact: Fei He (plane83@gamil.com)

1)network_data.mat
The detailed information of 134 coexpression networks are stored as MATLAB file.
ind1:	gene1
ind2:	gene2
GSE_id:	source
weight:	Pearson's correlation coefficient

2)gene_index.txt
The gene index in the MATLAB file.

3)dataset_index.txt
The dataset/source index in the MATLAB file. 


4)edge_PanNetwork.txt
The edge list in Pan Network is stored as text file for convenience


